var dataSet = [0, 1, 2, 3]

function onLoad() {
  d3.select("body")
    .selectAll("p")
    .data(dataSet)
    .enter()
    .append("p")
    .text(function (data) {
      return data
    })
}
